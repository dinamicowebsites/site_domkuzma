<?php
/**
 * @file
 * dkuz_content.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function dkuz_content_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Predstavitev',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'c48824f4-77f7-43f5-8902-797509b061b7',
  'type' => 'page',
  'language' => 'und',
  'created' => 1393717855,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '2e316adf-fd0d-4e9a-9b9f-b06f4747d09b',
  'revision_uid' => 1,
  'body' => array(),
  'field_files' => array(),
  'field_image' => array(),
  'field_images' => array(),
  'field_related_content' => array(),
  'path' => array(
    'pathauto' => 1,
  ),
  'name' => 'Superadmin',
  'picture' => 0,
  'data' => 'b:0;',
  'pathauto_perform_alias' => FALSE,
  'date' => '2014-03-02 00:50:55 +0100',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Kontakt',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '03407650-7269-4288-9e08-e82cc17efb48',
  'type' => 'page',
  'language' => 'und',
  'created' => 1393717934,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '6bac5eb8-8b92-4ecf-a67a-ac34fc9f9deb',
  'revision_uid' => 1,
  'body' => array(),
  'field_files' => array(),
  'field_image' => array(),
  'field_images' => array(),
  'field_related_content' => array(),
  'path' => array(
    'pathauto' => 1,
  ),
  'name' => 'Superadmin',
  'picture' => 0,
  'data' => 'b:0;',
  'pathauto_perform_alias' => FALSE,
  'date' => '2014-03-02 00:52:14 +0100',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Storitve',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '4099d0d4-f5a5-4b7f-8ec9-5fbb3011bad9',
  'type' => 'page',
  'language' => 'und',
  'created' => 1393717919,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '8f1e7a40-9c09-49fc-a924-c2b09fe99a32',
  'revision_uid' => 1,
  'body' => array(),
  'field_files' => array(),
  'field_image' => array(),
  'field_images' => array(),
  'field_related_content' => array(),
  'path' => array(
    'pathauto' => 1,
  ),
  'name' => 'Superadmin',
  'picture' => 0,
  'data' => 'b:0;',
  'pathauto_perform_alias' => FALSE,
  'date' => '2014-03-02 00:51:59 +0100',
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Informacije',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '54cb4832-f827-4376-b797-2a51a4a0237d',
  'type' => 'page',
  'language' => 'und',
  'created' => 1393717900,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'd2091d44-f150-4b80-961c-aa15147272b7',
  'revision_uid' => 1,
  'body' => array(),
  'field_files' => array(),
  'field_image' => array(),
  'field_images' => array(),
  'field_related_content' => array(),
  'path' => array(
    'pathauto' => 1,
  ),
  'name' => 'Superadmin',
  'picture' => 0,
  'data' => 'b:0;',
  'pathauto_perform_alias' => FALSE,
  'date' => '2014-03-02 00:51:40 +0100',
);
  return $nodes;
}
