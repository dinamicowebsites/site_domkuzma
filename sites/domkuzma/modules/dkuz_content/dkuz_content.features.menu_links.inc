<?php
/**
 * @file
 * dkuz_content.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function dkuz_content_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_informacije:node/2
  $menu_links['main-menu_informacije:node/2'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'Informacije',
    'options' => array(
      'identifier' => 'main-menu_informacije:node/2',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: main-menu_kontakt:node/4
  $menu_links['main-menu_kontakt:node/4'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/4',
    'router_path' => 'node/%',
    'link_title' => 'Kontakt',
    'options' => array(
      'identifier' => 'main-menu_kontakt:node/4',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: main-menu_predstavitev:node/1
  $menu_links['main-menu_predstavitev:node/1'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'Predstavitev',
    'options' => array(
      'identifier' => 'main-menu_predstavitev:node/1',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: main-menu_storitve:node/3
  $menu_links['main-menu_storitve:node/3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Storitve',
    'options' => array(
      'identifier' => 'main-menu_storitve:node/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Informacije');
  t('Kontakt');
  t('Predstavitev');
  t('Storitve');


  return $menu_links;
}
