core = 7.x
api = 2

projects[contento_features][type] = "module"
projects[contento_features][download][type] = "get"
projects[contento_features][download][url] = "https://bitbucket.org/dinamico/contento_features/get/master.zip"


;projects[file_entity][version] = "2.0-alpha1"
;projects[file_entity][subdir] = "contrib"

;projects[media][version] = "2.0-alpha1"
;projects[media][subdir] = "contrib"
;projects[media][patch][] = "https://drupal.org/files/media.media-browser.1956620-10.patch"
;projects[media][patch][] = "https://drupal.org/files/media.code_.1701914-2.patch"
;projects[media][patch][] = "https://drupal.org/files/media-857362-selection-bookmarking.patch"

;projects[media_vimeo][version] = "1.0-beta5"
;projects[media_vimeo][subdir] = "contrib"

;projects[media_youtube][version] = "2.0-rc1"
;projects[media_youtube][subdir] = "contrib"


;projects[site_map][version] = "1.0"
;projects[site_map][subdir] = "contrib"
;projects[site_map][patch][] = "http://drupal.org/files/accessibility_enhancements-1920722-2.patch"


;projects[twitter_block][version] = "2.1"
;projects[twitter_block][subdir] = "contrib"

;projects[workbench][version] = "1.2"
;projects[workbench][subdir] = "contrib"

;projects[workbench_moderation][version] = "1.3"
;projects[workbench_moderation][subdir] = "contrib"